package com.aem.training.platform.models;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.aem.training.platform.objects.PageObject;

@ConsumerType
public interface SampleModel {

	public String getName();
	
	public String getAddress();
	
	public String getPhone();
	
	public List<PageObject> getChildPages();
	
}
