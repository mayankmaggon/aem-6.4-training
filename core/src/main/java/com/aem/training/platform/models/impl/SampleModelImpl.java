package com.aem.training.platform.models.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aem.training.platform.models.SampleModel;
import com.aem.training.platform.objects.PageObject;
import com.aem.training.platform.util.ProfileUtilService;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;

@Model(adaptables=Resource.class, adapters=SampleModel.class)
public class SampleModelImpl implements SampleModel {

	private static final Logger log = LoggerFactory.getLogger(SampleModelImpl.class);

	@Inject
	ProfileUtilService profileUtilService;
	
	@Inject
	ResourceResolver resourceResolver;
	
	@Inject @Optional
	private String firstName;
	
	@Inject @Optional
	private String address;
	
	@Inject @Optional
	private String phone;
	
	@Inject @Optional
	@Default(values="{}")
	private String[] childPages;
	
	private String salary;
	
	private List<PageObject> childPageList;
	
	String pagePath = "/content/we-retail/language-masters/en/products/men/coats";
	
	@PostConstruct
	public void init() {
		
		try {
			//firstName = firstName.toUpperCase();
			//address = address.toUpperCase();
			//phone = "+91-" + phone;	
			
			childPageList = new ArrayList<>();
			
			/*Resource pageResource = resourceResolver.getResource(pagePath);
			if((pageResource != null) && (pageResource instanceof Resource)) {
				log.info("Got Page Resource :: " + pagePath);
				Iterable<Resource> childPages = pageResource.getChildren();
				for(Resource childResource : childPages) {
					if(!childResource.getName().equalsIgnoreCase("jcr:content")) {
						Page childPage = childResource.adaptTo(Page.class);
						if(childPage != null) {
							log.info("Found Child Page :: " + childPage.getName());
							ValueMap pageProperties = childPage.getProperties();
							String title = childPage.getTitle();
							String image = (String) pageProperties.get("coverImage");
							String url = childPage.getPath();
							
							PageObject pageObject = new PageObject();
							pageObject.setTitle(title);
							pageObject.setUrl(url);
							pageObject.setImage(image);
							
							childPageList.add(pageObject);
						}
					}
				}
			}*/
			
			
			Gson gson = new Gson();
			for(String jsonString : childPages) {
				
				log.info("JSON String :: " + jsonString);
				
				PageObject pageObject = gson.fromJson(jsonString, PageObject.class);
				childPageList.add(pageObject);
			}
			
			log.info("Size of List :: " + childPageList.size());
		} catch(Exception e) {
			log.error("Exception in init method of Sample Model :: " + e.getMessage());
		}
	}
	
	@Override
	public String getName() {
		return firstName;
	}
	
	@Override
	public String getAddress() {
		return address;
	}
	
	@Override
	public String getPhone() {
		return phone;
	}
	
	@Override
	public List<PageObject> getChildPages(){
		return childPageList;
	}
	
}
