package com.aem.training.platform.models.templates.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.training.platform.models.templates.BasePageModel;

@Model(adaptables=Resource.class,adapters=BasePageModel.class)
public class BasePageModelImpl implements BasePageModel {
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(BasePageModelImpl.class);
	
	@Inject @Optional
	@Named("jcr:title")
	private String title;
	
	@PostConstruct
	public void init() {
		log.info("Theory of Everything");
	}

	@Override
	public String getTitle() {
		return title;
	}

}
