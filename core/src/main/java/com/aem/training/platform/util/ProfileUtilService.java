package com.aem.training.platform.util;

public interface ProfileUtilService {

	public String getSalary(String designation, String department);
	
}
