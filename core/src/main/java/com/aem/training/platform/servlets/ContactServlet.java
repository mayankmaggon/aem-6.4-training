package com.aem.training.platform.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
		immediate = true,
		service= {Servlet.class},
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Contact Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths="+ "/bin/contact"
		})
public class ContactServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SampleServlet.class);

	@Activate
	protected void activate() {
		log.info("Contact Servlet activated!");
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		PrintWriter out = response.getWriter();
		
		String name = "";
		String email = "";
		String phone = "";
		String message = "";
		
		if(request.getParameter("name") != null) {
			name = request.getParameter("name");
		}
		
		if(request.getParameter("email") != null) {
			email = request.getParameter("email");
		}
		
		if(request.getParameter("phone") != null) {
			phone = request.getParameter("phone");
		}
		
		if(request.getParameter("message") != null) {
			message = request.getParameter("message");
		}
		
		out.println("Thank you for contacting\n");
		out.println("Summary \n");
		out.println("Name :: " + name);
		out.println("Email :: " + email);
		out.println("Phone :: " + phone);
		out.println("Message :: " + message);
		
	}
	
}
