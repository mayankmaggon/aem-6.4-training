package com.aem.training.platform.util;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;

import com.aem.training.platform.services.SampleService;

@Component(
		immediate = true,
		service = {ProfileUtilService.class},
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Profile Util Service"
 		})
public class ProfileUtilServiceImpl implements ProfileUtilService {
	
	@Override
	public String getSalary(String designation, String department) {
		String salary = "100";
		
		if(designation.equals("developer") && department.equals("development")) {
			salary = "10000";
		} else if (designation.equals("manager") && department.equals("development")) {
			salary = "20000";
		}
		return salary;
	}
	
}
