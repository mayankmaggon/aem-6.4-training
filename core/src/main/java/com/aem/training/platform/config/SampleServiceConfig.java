package com.aem.training.platform.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Sample Service Configuration",description="Config Sample Service")
public @interface SampleServiceConfig {
	
	@AttributeDefinition(name="Sample Service Name",description="")
	String getName() default "";
	
	@AttributeDefinition(name="Sample List of Names",description="")
	String[] listofNames() default {};
	
	@AttributeDefinition(name="Enabled",description="")
	boolean enabled() default false;

}
