package com.aem.training.platform.models.templates;

public interface BasePageModel {
	
	public String getTitle();

}
