package com.aem.training.platform.services.impl;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.training.platform.config.SampleServiceConfig;
import com.aem.training.platform.services.SampleService;

@Component(
		immediate = true,
		service = {SampleService.class},
		enabled = true,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=" + "Sample Service"
 		})
@Designate(ocd=SampleServiceConfig.class)
public class SampleServiceImpl implements SampleService {
	
	/** Logger. **/
	private static final Logger log = LoggerFactory.getLogger(SampleServiceImpl.class);

	private String name;
	private boolean enabled;
	private String[] listOfNames;
	
	@Activate
	public void activate(SampleServiceConfig config) {
		log.info("Sample Service Activated");
		
		this.name = config.getName();
		this.enabled = config.enabled();
		this.listOfNames = config.listofNames();
		
		log.info("Sample Service :: " + name);
		
	}
}
